package com.rachel.pickerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * author zhengshaorui
 * 该项目主要用于 Android Tv 的数量选择
 */
public class MainActivity extends AppCompatActivity {

    private PickerView mPickerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPickerView = (PickerView) findViewById(R.id.pickerview);
        List<String> lists = new ArrayList<>();
        for (int i = 2007; i < 2030; i++) {
            lists.add(i+"");
        }

        mPickerView.setData(lists,"2017");//后面这个是初始化是要显示的数值
    }

    //可使用键盘的上下键实现滚动
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
           mPickerView.setPickerViewMove(PickerView.SCROLL_DOWN);
        }
        if (keyCode == KeyEvent.KEYCODE_DPAD_UP){
            mPickerView.setPickerViewMove(PickerView.SCROLL_UP);
        }
        return super.onKeyDown(keyCode, event);
    }
}
