# PickerView
用来Android TV 的 pickerview 滚动

在使用的时候，可以使用键盘上的上下来实现滚动

我实现的效果如下：

![](https://user-gold-cdn.xitu.io/2017/7/9/6105868353a1a43e580057c2c061fe10)



现在上传的是简单demo，布局的优化自己去实现。

#### 关于 popupwindow 的封装可以看这篇文章： https://juejin.im/post/5961e03e51882568b13c3308

## how to use

首先时布局：

```
 <com.rachel.pickerview.PickerView
        android:id="@+id/pickerview"
        android:layout_centerInParent="true"
        android:background="@color/colorAccent"
        android:layout_width="90dp"
        android:clickable="true"
        android:layout_height="300dp"
        />
```

然后在 主函数中这样调用即可：
```
public class MainActivity extends AppCompatActivity {

    private PickerView mPickerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPickerView = (PickerView) findViewById(R.id.pickerview);
        List<String> lists = new ArrayList<>();
        for (int i = 2007; i < 2030; i++) {
            lists.add(i+"");
        }

        mPickerView.setData(lists,"2017");//后面这个是初始化是要显示的数值
    }

    //可使用键盘的上下键实现滚动
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
           mPickerView.setPickerViewMove(PickerView.SCROLL_DOWN);
        }
        if (keyCode == KeyEvent.KEYCODE_DPAD_UP){
            mPickerView.setPickerViewMove(PickerView.SCROLL_UP);
        }
        return super.onKeyDown(keyCode, event);
    }
}
```